#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages


from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

import sys, logging, requests, time, math


# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO) # Dans l'exercice des TPG c'est level=logging.DEBUG

logger = logging.getLogger(__name__)

CHOIX, RESTAURANTS, SORTIES, DETAILS_RESTAURANT, RETOUR, LIEU, COORDONNEES, DETAILS, RETOUR_RESTO = range(9)

##################### PARTIE LOISIRS #############################

# message de bienvenu
def start(bot, update):
    reply_keyboard = [['Sorties', 'Restaurants']]
    update.message.reply_text(
        'Bonjour {}, je suis Super_bot. Je suis là pour te renseigner sur les sorties ou sur les restaurants à Genève. \n\n'
        '(Tu peux écrire /sortir pour arrêter la conversation à tout moment).\n\n'
        'Sur quel sujet veux-tu avoir des renseignements?\n\n'.format(update.message.from_user.first_name),
    reply_markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return CHOIX

# affiche les différents types de restaurants
def restaurants_choix(bot, update):
    reply_keyboard_restaurant= [
        ['Asiatique', 'Italien', 'Portugais'],
        ['Fast-food', 'Mexicain'],
        ['Menu principal']
    ]
    update.message.reply_text(
        'Quel type de cuisine aimes-tu?',
        reply_markup = ReplyKeyboardMarkup(
            reply_keyboard_restaurant,
            one_time_keyboard=True
        )
    )

    return RESTAURANTS

# affiche les différents types de sorties
def sortir_choix(bot, update):
    reply_keyboard_sorties = [
        ['Musées', 'Bars', 'Clubs'],
        ['Menu principal']
    ]
    update.message.reply_text(
        'Quel type de sorties recherches-tu?',
        reply_markup = ReplyKeyboardMarkup(
            reply_keyboard_sorties,
            one_time_keyboard=True
        )
    )

    return SORTIES

# affiche le liste des restaurants selon le type de nourriture choisi (ici c'est la même liste pour tout type choisi)
def restau_resultats(bot, update):
    reply_keyboard_liste_restaurant = [
        ['Restaurant les Palettes', 'Restaurant Le Marignac'],
        ['Pizzaria Luigi','Restaurant Carrefour'],
        ['Wasabi', 'Restaurant Los Bandidos'],
        ['Nouvelle recherche']
    ]
    update.message.reply_text(
        'Voici la listes des restaurants: \n\n',
        reply_markup = ReplyKeyboardMarkup(
            reply_keyboard_liste_restaurant,
            one_time_keyboard=True
        )
    )

    return DETAILS_RESTAURANT

# affiche le détail du restaurant choisi
def restau_details(bot, update):
    reply_keyboard_retour_restaurant = [
        ['Autres restaurants']
    ]
    update.message.reply_text(
        'Nom: {} \n\n'
        'Note: 4,7 sur 5 étoiles\n'
        'Prix moyen: 25 - 35 CHF \n'
        'Horaire d\'aujourd\'hui: 11:00 - 23:00\n'
        'Adresse : Boulevard du Pont-d\'Arve 48, 1205 Genève\n'.format(update.message.text),
    )
    update.message.reply_location(46.1948507, 6.1420473,
    reply_markup = ReplyKeyboardMarkup(
        reply_keyboard_retour_restaurant,
        one_time_keyboard=True
    )
    )

    return RETOUR_RESTO

# affiche le top 3 des musées
def liste_musees(bot, update):
    update.message.reply_text(
        '1) Musée Rath \n\n'
        'Adresse: Place de Neuve 1, 1204 Genève\n'
        'Horaire d\'aujourd\'hui: 11:00 - 18:00\n'
        'Prix: Gratuit\n '
    )
    update.message.reply_text(
        '2) Musée d\'art et d\'histoire\n'
        'Adresse: Rue Charles-Galland 2 1206 Genève \n'
        'Horaire d\'aujourd\'hui: 11:00 - 18:00\n'
        'Prix: Gratuit\n '
    )
    reply_keyboard_retour = [
        ['Retour']
    ]
    update.message.reply_text(
        '3) Musée d\'éthnographie de Genève\n\n'
        'Adresse: Boulevard Carl-Vogt 65-67, 1205 Genève\n'
        'Horaire d\'aujourd\'hui: 11:00 - 18:00\n'
        'Prix: Gratuit\n ',
        reply_markup = ReplyKeyboardMarkup(
            reply_keyboard_retour,
            one_time_keyboard=True
        )
    )
    return RETOUR

# affiche le top 3 des bars
def liste_bars(bot, update):
    update.message.reply_text(
        '1) Living Room Bar & Kitchen \n\n'
        'Adresse: Quai du Mont-Blanc 11, 1201 Geneva 1\n'
        'Horaire d\'aujourd\'hui: 19:00 - 22:00\n'
        'Prix moyen: 25 CHF\n ',
    )
    update.message.reply_text(
        '2) Bottle Brothers - Grand Bottle \n\n'
        'Adresse: Rue Henri-Blanvalet 12, 1207 Geneva\n'
        'Horaire d\'aujourd\'hui: 17:00 - 00:00\n'
        'Prix moyen: 15 CHF\n ',
    )
    reply_keyboard_retour = [
        ['Retour']
    ]
    update.message.reply_text(
        '3) Le Kraken Bar \n\n'
        'Adresse: Rue de l\'Ecole-de-Médecine 8, 1205 Genève\n'
        'Horaire d\'aujourd\'hui: 13:00 - 1:30\n'
        'Prix moyen: 15 CHF\n',
        reply_markup = ReplyKeyboardMarkup(
            reply_keyboard_retour,
            one_time_keyboard=True
        )
    )
    return RETOUR

# affiche le top 3 des clubs
def liste_clubs(bot, update):
    update.message.reply_text(
        '1) Java Club\n\n'
        'Adresse :Quai du Mont-Blanc 19, 1201 Genève\n'
        'Horaire d\'aujourd\'hui: 23:00 - 5:00\n'
        'Prix d\'entrée : 50 CHF')
    update.message.reply_text(
        '2) The Baroque Club\n\n'
        'Adresse :Place de la Fusterie 12, 1204 Genève\n'
        'Horaire d\'aujourd\'hui: 00:00 - 04:00\n'
        'Prix d\'entrée : 40 CHF')
    reply_keyboard_retour = [
        ['Retour']
    ]
    update.message.reply_text(
        '3) Moulin Rouge Geneva\n\n'
        'Adresse :Avenue du Mail 1, 1205 Genève\n'
        'Horaire d\'aujourd\'hui: 22:00 - 05:30\n'
        'Prix d\'entrée : 50 CHF',
        reply_markup=ReplyKeyboardMarkup(
            reply_keyboard_retour,
            one_time_keyboard=True
        )
    )

    return RETOUR

# fini le programme
def sortir(bot, update):
    update.message.reply_text('Aurevoir et à bientôt!',
                              reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END

##################### PARTIE TRANSPORT #############################

##### Informations #####

#Récupèration des données sur Opendata
def appeler_opendata(path):
    url = "http://transport.opendata.ch/v1/" + path
    reponse = requests.get(url)
    return reponse.json()

#Indication du temps restant
def calcul_temps_depart(timestamp):
    seconds = timestamp-time.time()
    minutes = math.floor(seconds/60)
    if minutes < 1:
        return "FAUT COURIR!"
    if minutes > 60:
        return "> {} h.".format(math.floor(minutes/60))
    return "dans {} min.".format(minutes)


##### Préparation des messages #####

#Envoie des résultats des station autour
def afficher_arrets(update, arrets):
    texte_de_reponse = "Voici les arrêts:\n"
    for station in arrets['stations']:
        if station['id'] is not None:
            texte_de_reponse += "\n/a" + station['id'] + " " + station['name']
    update.message.reply_text(texte_de_reponse)

#Detail de la station
def afficher_departs(update, departs):
    texte_de_reponse = "Voici les prochains départs:\n\n"
    for depart in departs['stationboard']:
        texte_de_reponse += "{} {} dest. {} - {}\n".format(
            depart['category'],
            depart['number'],
            depart['to'],
            calcul_temps_depart(depart['stop']['departureTimestamp'])
        )
    texte_de_reponse += "\nAfficher a nouveau: /a" + departs['station']['id']

    coordinate = departs['station']['coordinate']
    update.message.reply_location(coordinate['x'], coordinate['y'])
    update.message.reply_text(texte_de_reponse)


##### Réponses #####

#Message d'accueil
def bienvenu(bot, update):
    update.message.reply_text("Bonjour {}, je suis là pour te renseigner sur les TPG.\n"
                              "Merci d'envoyer ta localisation (via ta localisation ou simplement en texte"
                              .format(update.message.from_user.first_name))
    return LIEU


#Affiche la liste des stations aux allentours
def lieu_a_chercher(bot, update):
    resultats_opendata = appeler_opendata("locations?query=" + update.message.text)
    afficher_arrets(update, resultats_opendata)

    return COORDONNEES

# recherche par location
def coordonnees_a_traiter(bot, update):
    location = update.message.location
    resultats_opendata = appeler_opendata("locations?x={}&y={}".format(location.latitude, location.longitude))
    afficher_arrets(update, resultats_opendata)

    return DETAILS


def details_arret(bot, update):
    arret_id = update.message.text[2:]
    afficher_departs(update, appeler_opendata("stationboard?id=" + arret_id))


############ FONCTION MAIN  ################
def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(sys.argv[1])

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO

# PARTIE LOISIRS
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            CHOIX: [
                RegexHandler('Sorties', sortir_choix),
                RegexHandler('Restaurants', restaurants_choix),
                CommandHandler('sortir', sortir),
                RegexHandler('', start) # si la personne écrit quelque chose cela retourne à la page d'avant (comme ca la personne n'est pas bloquée)
            ],

            RESTAURANTS: [
                RegexHandler('Asiatique', restau_resultats),
                RegexHandler('Italien', restau_resultats),
                RegexHandler('Portugais', restau_resultats),
                RegexHandler('Fast-food', restau_resultats),
                RegexHandler('Mexicain', restau_resultats),
                RegexHandler('Menu principal', start),
                CommandHandler('sortir', sortir),
                RegexHandler('', restaurants_choix) # si la personne écrit quelque chose cela re affiche la question (comme ca la personne n'est pas bloquée)

            ],

            SORTIES: [
                RegexHandler('Musées', liste_musees),
                RegexHandler('Bars', liste_bars),
                RegexHandler('Clubs', liste_clubs),
                RegexHandler('Menu principal', start),
                CommandHandler('sortir', sortir),
                RegexHandler('',sortir_choix) # si la personne écrit quelque chose cela re affiche la question (comme ca la personne n'est pas bloquée)
            ],

            DETAILS_RESTAURANT: [
                RegexHandler('Restaurant les Palettes', restau_details),
                RegexHandler('Restaurant Le Marignac', restau_details),
                RegexHandler('Pizzaria Luigi', restau_details),
                RegexHandler('Restaurant Carrefour', restau_details),
                RegexHandler('Wasabi', restau_details),
                RegexHandler('Restaurant Los Bandidos', restau_details),
                RegexHandler('Nouvelle recherche', restaurants_choix),
                CommandHandler('sortir', sortir),
                RegexHandler('', restau_resultats) # si la personne écrit quelque chose cela re affiche la question (comme ca la personne n'est pas bloquée)
            ],

            RETOUR: [
                RegexHandler('Retour',sortir_choix),
                CommandHandler('sortir', sortir),
                RegexHandler('', sortir_choix) # si la personne écrit quelque chose cela retourne à la page avec le choix des sortie (comme ca la personne n'est pas bloquée)

            ],

            RETOUR_RESTO: [
                RegexHandler('Autres restaurants', restau_resultats),
                CommandHandler('sortir', sortir),
                RegexHandler('', restau_resultats) # si la personne écrit quelque chose cela retourne à la page avec le nom des différents restaurants (comme ca la personne n'est pas bloquée)

            ]
        },

        fallbacks=[]
    )

# PARTIE TRANSPORT
    conv_handler_transport = ConversationHandler(
        entry_points = [CommandHandler('transport', bienvenu)],

        states={

            LIEU: [
                MessageHandler(Filters.text, lieu_a_chercher)
            ],

            COORDONNEES: [
                MessageHandler(Filters.location, coordonnees_a_traiter)
            ],

            DETAILS: [
                MessageHandler(Filters.command, details_arret)
            ]

        },

        fallbacks=[CommandHandler('sortir', sortir)]
    )


    dp.add_handler(conv_handler)
    dp.add_handler(conv_handler_transport)



    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()